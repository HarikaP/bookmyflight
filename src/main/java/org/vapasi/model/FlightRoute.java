package org.vapasi.model;

import java.util.Date;

public class FlightRoute {
    private int flightID;

    public String getFlightName() {
        return flightName;
    }

    public void setFlightName(String flightName) {
        this.flightName = flightName;
    }

    private String flightName;
    private double economyBasePrice;
    private double businessBasePrice;

    public double getEconomyBasePrice() {
        return economyBasePrice;
    }

    public void setEconomyBasePrice(double economyBasePrice) {
        this.economyBasePrice = economyBasePrice;
    }

    public double getBusinessBasePrice() {
        return businessBasePrice;
    }

    public void setBusinessBasePrice(double businessBasePrice) {
        this.businessBasePrice = businessBasePrice;
    }

    public double getFirstClassBasePrice() {
        return firstClassBasePrice;
    }

    public void setFirstClassBasePrice(double firstClassBasePrice) {
        this.firstClassBasePrice = firstClassBasePrice;
    }

    private double firstClassBasePrice;
    private double totalFare;
    private Date departureDate;

    public int getFlightID() {
        return flightID;
    }

    public void setFlightID(int flightID) {
        this.flightID = flightID;
    }


    public double getTotalFare() {
        return totalFare;
    }

    public void setTotalFare(double totalFare) {
        this.totalFare = totalFare;
    }

    public Date getDepartureDate() {
        return departureDate;
    }

    public void setDepartureDate(Date departureDate) {
        this.departureDate = departureDate;
    }

    //FlightDetails flightDetails = new FlightDetails();
    public FlightRoute(int flightID,Date departureDate,double economyBasePrice,double businessBasePrice,double firstClassBasePrice)
    {
        this.flightID = flightID;
        this.economyBasePrice = economyBasePrice;
        this.businessBasePrice = businessBasePrice;
        this.firstClassBasePrice = firstClassBasePrice;
        this.departureDate = departureDate;
    }
    public FlightRoute(int flightID,Date departureDate,double economyBasePrice)
    {
        this.flightID = flightID;
        this.economyBasePrice = economyBasePrice;
        this.departureDate = departureDate;
    }
    public FlightRoute(int flightID,Date departureDate,double economyBasePrice,double businessBasePrice)
    {
        this.flightID = flightID;
        this.economyBasePrice = economyBasePrice;
        this.businessBasePrice = businessBasePrice;
        this.departureDate = departureDate;
    }



}

package org.vapasi.model;

import java.time.LocalDate;
import java.util.Date;

public class FlightDetails {

    private int flightID;
    private String source;
    private String destination;
    private int noOfPassengers;
    private String flightName;
    private String classOfTravel;
    private double basePrice;
    private double totalFare;
    private Date departureDate;


    public FlightDetails(int flightID,String source,String destination,String flightName,Date departureDate){
        this.flightID = flightID;
        this.source = source;
        this.destination = destination;
        this.flightName = flightName;
        this.departureDate = departureDate;
    }

    public int getFlightID() {
        return flightID;
    }

    public void setFlightID(int flightID) {
        this.flightID = flightID;
    }

    public double getTotalFare() {
        return totalFare;
    }

    public void setTotalFare(double totalFare) {
        this.totalFare = totalFare;
    }

    public Date getDepartureDate() {
        return departureDate;
    }

    public void setDepartureDate(Date departureDate) {
        this.departureDate = departureDate;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public int getNoOfPassengers() {
        return noOfPassengers;
    }

    public void setNoOfPassengers(int noOfPassengers) {
        this.noOfPassengers = noOfPassengers;
    }

    public String getFlightName() {
        return flightName;
    }

    public void setFlightName(String flightName) {
        this.flightName = flightName;
    }

    public String getClassOfTravel() {
        return classOfTravel;
    }

    public void setClassOfTravel(String classOfTravel) {
        this.classOfTravel = classOfTravel;
    }

    public double getBasePrice() {
        return basePrice;
    }

    public void setBasePrice(double basePrice) {
        this.basePrice = basePrice;
    }

    @Override
    public String toString() {
        return "FlightDetails{" +
                "source='" + source + '\'' +
                ", destination='" + destination + '\'' +
                ", flightName='" + flightName + '\'' +
                "departureDate='" + departureDate + '\'' +
                ", classOfTravel='" + classOfTravel + '\'' +
                ", noOfPassengers='" + noOfPassengers + '\'' +
                '}';
    }

}

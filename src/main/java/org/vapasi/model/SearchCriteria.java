package org.vapasi.model;

import javax.validation.constraints.NotBlank;
import java.util.Calendar;
import java.util.Date;

public class SearchCriteria {
    private int noOfPassengers;
    @NotBlank(message = "source cannot be empty")
    private String source;
    @NotBlank(message = "source cannot be empty")
    private String destination;
    private String classOfTravel;
    private Date departureDate;

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public int getNoOfPassengers() {
        return noOfPassengers;
    }

    public void setNoOfPassengers(int noOfPassengers) {
        this.noOfPassengers = noOfPassengers;
    }

    public String getClassOfTravel() {
        return classOfTravel;
    }

    public void setClassOfTravel(String classOfTravel) {
        this.classOfTravel = classOfTravel;
    }

    public Date getDepartureDate() {


        return departureDate;
    }

    public void setDepartureDate(Date departureDate) {

        this.departureDate = departureDate;
      /*  Calendar cal = Calendar.getInstance();
        cal.setTime(departureDate);
        String formatedDate = cal.get(Calendar.DATE) + "/" +
                (cal.get(Calendar.MONTH) + 1) +
                "/" +         cal.get(Calendar.YEAR);
        System.out.println(formatedDate);*/
    }



}

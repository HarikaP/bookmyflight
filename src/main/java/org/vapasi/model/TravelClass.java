package org.vapasi.model;

public class TravelClass {
    private String flightName;
    private String classOfTravel;
    private int totalNoOfSeats;
    private double basePrice;

    public int getRemainingNoOfSeats() {
        return remainingNoOfSeats;
    }

    public void setRemainingNoOfSeats(int remainingNoOfSeats) {
        this.remainingNoOfSeats = remainingNoOfSeats;
    }

    private int remainingNoOfSeats;


    public double getBasePrice() {
        return basePrice;
    }

    public void setBasePrice(double basePrice) {
        this.basePrice = basePrice;
    }



    public String getFlightName() {
        return flightName;
    }

    public TravelClass(String flightName, String classOfTravel, int totalNoOfSeats,int basePrice,int remainingNoOfSeats) {
        this.flightName = flightName;
        this.classOfTravel = classOfTravel;
        this.totalNoOfSeats = totalNoOfSeats;
        this.basePrice = basePrice;
        this.remainingNoOfSeats = remainingNoOfSeats;
    }

    public void setFlightName(String flightName) {
        this.flightName = flightName;
    }

    public String getClassOfTravel() {
        return classOfTravel;
    }

    public void setClassOfTravel(String classOfTravel) {
        this.classOfTravel = classOfTravel;
    }

    public int getTotalNoOfSeats() {
        return totalNoOfSeats;
    }

    public void setTotalNoOfSeats(int totalNoOfSeats) {
        this.totalNoOfSeats = totalNoOfSeats;
    }


}

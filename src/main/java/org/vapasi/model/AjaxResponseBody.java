package org.vapasi.model;

import java.util.List;
import java.util.Map;

public class AjaxResponseBody {
    String msg;
    List<FlightDetails> result;

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public List<FlightDetails> getResult() {
        return result;
    }

    public void setResult(List<FlightDetails> result) {
        this.result = result;
    }
}

package org.vapasi.controller;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.vapasi.model.AjaxResponseBody;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.vapasi.model.FlightDetails;
import org.vapasi.model.SearchCriteria;
import org.vapasi.service.SearchService;

import javax.validation.Valid;
import java.text.ParseException;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@RestController
public class FlightController {


  /*  @PostMapping("/flight/search")
    public ResponseEntity<Map<FlightDetails, String>> returnFlightsWithPassengers(@RequestParam("origin") String origin, @RequestParam("destination") String destination, @RequestParam("noOfPassengers") int noOfPassengers, @RequestParam("departureDate") @DateTimeFormat(pattern="MMddyyyy") Date departureDate, @RequestParam("classOfTravel") String classOfTravel)
    {

        Map<FlightDetails,String> map= searchService.returnFlights(origin,destination,noOfPassengers,departureDate,classOfTravel);
        return ResponseEntity.ok(map);
    }
*/

    @Autowired
   SearchService searchService;

    /*@Autowired
    public void setSearchService(SearchService searchService) {
        this.searchService = searchService;

    }*/

    @PostMapping("/flights/search")
    public ResponseEntity<AjaxResponseBody> getSearchResultViaAjax(@Valid @RequestBody SearchCriteria search, Errors errors){
        AjaxResponseBody result = new AjaxResponseBody();


        //If error, just return a 400 bad request, along with the error message
        if (errors.hasErrors()) {

            result.setMsg(errors.getAllErrors().stream().map(x -> x.getDefaultMessage()).collect(Collectors.joining(",")));
            return ResponseEntity.badRequest().body(result);

        }

        List<FlightDetails> flights = searchService.returnFlights(search.getSource(),search.getDestination(),search.getNoOfPassengers(),search.getDepartureDate(),search.getClassOfTravel());

        if (flights.isEmpty()) {
            result.setMsg("no flight found!");
        } else {
            result.setMsg("success");
        }

        result.setResult(flights);


        return ResponseEntity.ok(result);

    }

}

package org.vapasi.service;

import org.vapasi.model.FlightDetails;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.vapasi.model.TravelClass;
import org.vapasi.repository.FlightRepository;
import java.time.temporal.ChronoUnit;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.*;


@Service
public class SearchService {
    public SearchService() throws ParseException {

    }
    private static int noOfSeatsFilled;
    private static int seats;

    public static Date addDays(Date date, int days)
    {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.DATE, days); //minus number would decrement the days
        return cal.getTime();
    }

    FlightRepository rep;

    @Autowired
    public void setRep(FlightRepository rep) {
        this.rep = rep;
    }


    List<FlightDetails> newFlight;
    public String getDateInFormat(Date someDate){
        Calendar cal = Calendar.getInstance();
        cal.setTime(someDate);
        String formattedDate = cal.get(Calendar.DATE) + "/" +
                (cal.get(Calendar.MONTH) + 1) +
                "/" + cal.get(Calendar.YEAR);
        return formattedDate;
    }
   /* public double evaluateBasePrice(String travelClass){

    }*/
    public List<FlightDetails> returnFlights(String source, String destination, int noOfPassengers, Date departureDate,String classOfTravel)
    {
        //TravelClass travelClass ;
        List<FlightDetails> flights = rep.retrieveSourceDestinationMapping();
        newFlight = new ArrayList<FlightDetails>();
        List<TravelClass> travelClassList = rep.retrieveTravelClasses();
        Date dateobj = new Date();
        Date nextDay = addDays(dateobj,1);

        for(FlightDetails f:flights) {
            if (source.equals(f.getSource()) && destination.equals(f.getDestination())) {
                if (classOfTravel == null) {
                    classOfTravel = "economy";
                }
                if (departureDate == null) {
                    departureDate = nextDay;
                }
                if (noOfPassengers == 0) {
                    noOfPassengers = 1;
                }
                String formattedDate = getDateInFormat(departureDate);
                String formattedDate1 = getDateInFormat(f.getDepartureDate());

                if (noOfPassengers >= 1 && departureDate.after(dateobj) && formattedDate.equals(formattedDate1)) {
                    newFlight.add(f);
                    for (TravelClass t : travelClassList) {
                        if (f.getFlightName().equals(t.getFlightName()) && classOfTravel.equals(t.getClassOfTravel())) {
                            seats = t.getRemainingNoOfSeats();
                            if(noOfPassengers<seats){
                                f.setClassOfTravel(classOfTravel);
                                f.setNoOfPassengers(noOfPassengers);
                                if(classOfTravel.equals("economy")){
                                    double basePriceValue = basePriceCalculationForEconomy(f.getNoOfPassengers(),t.getTotalNoOfSeats(),t.getBasePrice());
                                    f.setBasePrice(basePriceValue);;
                                }
                                else if(classOfTravel.equals("Business")){
                                    double basePriceValue = basePriceCalculatorForBusiness(f.getDepartureDate(),t.getBasePrice());
                                    f.setBasePrice(basePriceValue);
                                }
                                else if(classOfTravel.equals("FirstClass")){
                                    Date today = new Date();
                                    Date nDate = addDays(departureDate,-10);
                                    if(today.after(nDate)){
                                        double basePriceValue = basePriceCalculationForFirstClass(f.getDepartureDate(),t.getBasePrice());
                                        f.setBasePrice(basePriceValue);
                                    }
                                    else
                                    {
                                        System.out.println("Invalid date selected for First class");
                                        newFlight.remove(f);
                                    }
                                }
                                else
                                {
                                    System.out.println("Invalid class of travel");
                                    newFlight.remove(f);
                                }
                                f.setTotalFare(costOfBooking(f));
                            }
                        }
                    }
                    if (f.getClassOfTravel() == null || !(f.getClassOfTravel().equals(classOfTravel))) {
                        newFlight.remove(f);
                    }
                }
                else
                    {
                    continue;
                }
            }
        }
        //System.out.println(newFlight);
        //costOfBooking();
        return newFlight;
    }

    private double basePriceCalculationForFirstClass(Date date,double basePrice) {
            Date bookingDate = new Date();
            long diff = (date.getTime() - bookingDate.getTime())/(1000*60*60*24);

            for(double n=0;n<=10;n++){
                if(diff==n){
                    double x = 1+((10-n)/10);
                    System.out.println(x);
                    basePrice = x * basePrice;
                }
            }
        return basePrice;
    }

    private double basePriceCalculatorForBusiness(Date date,double basePrice) {
        //Date now = new Date();
        Calendar cal = Calendar.getInstance();

        cal.setTime(date);
        String dateString = cal.toString();
        System.out.println();
        int day = cal.get(Calendar.DAY_OF_WEEK);
        System.out.println(day);
       /* String formatedDate = cal.get(Calendar.DATE) + "/" +
                (cal.get(Calendar.MONTH) + 1) +
                "/" + cal.get(Calendar.YEAR);
        SimpleDateFormat simpleDateformat = new SimpleDateFormat("Monday"); // the day of the week abbreviated
        System.out.println(simpleDateformat.format(date));
        String day = simpleDateformat.format(date);*/
        if(day == 2|| day == 6|| day == 1){
            basePrice =  1.4*basePrice;
        }

        return basePrice;

    }

    public double basePriceCalculationForEconomy(int noOfPassengers,int totalNoOfSeats,double basePrice){

        noOfSeatsFilled = noOfPassengers + (totalNoOfSeats-seats);
        System.out.println("No of seats filled:" +noOfSeatsFilled);
        seats = totalNoOfSeats-noOfSeatsFilled;
        System.out.println(seats);
        if(seats>(0.6*totalNoOfSeats)){
            basePrice = basePrice;
        }
        else if((seats>(0.1*totalNoOfSeats)) && (seats<=(0.6*totalNoOfSeats))){
            basePrice = basePrice +(.3*basePrice);
        }
        else {
            basePrice = basePrice + (.6*basePrice);
        }
        System.out.println("Base Price:" +basePrice);
        return basePrice;
    }

    public double costOfBooking(FlightDetails f){
        double totalFare = 0;
       /* if(c.getRemainingNoOfSeats()<(0.6*c.getNoOfSeats()))
        {
            f.setBasePrice(f.getBasePrice());
        }
        else if((c.getRemainingNoOfSeats()<(.1*c.getNoOfSeats()))&&c.getRemainingNoOfSeats()>=(0.6*c.getNoOfSeats()))
        {
            f.setBasePrice(1.3*f.getBasePrice());
        }
        else {
            f.setBasePrice(1.6*f.getBasePrice());
        }*/

 /*      if(remainingNoOfSeats>=(0.6*c.getTotalNoOfSeats())){
           f.setBasePrice(f.getBasePrice());
       }
       else if(remainingNoOfSeats<(0.6*c.getTotalNoOfSeats())&&remainingNoOfSeats>(0.9*c.getTotalNoOfSeats())){
           f.setBasePrice(f.getBasePrice()*1.3);
       }
       else
       {
           f.setBasePrice(f.getBasePrice()*1.6);
       }*/
        totalFare = totalFare +(f.getNoOfPassengers()*f.getBasePrice());
        System.out.println(totalFare);
        return  totalFare;
    }

}

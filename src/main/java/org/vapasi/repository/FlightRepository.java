package org.vapasi.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.vapasi.model.FlightDetails;
import org.springframework.stereotype.Repository;
import org.vapasi.model.FlightRoute;
import org.vapasi.model.TravelClass;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

@Repository
public class FlightRepository {
    @Autowired
    JdbcTemplate jdbcTemplate;

    private List<String> flight,city;
    private List<FlightDetails> sourceDestinationFlights;
    List<TravelClass> travelClassList = new ArrayList<>();

    //public String[] classofTravel = {"Economy","Business","FirstClass"};
    String sDate1="31/08/2019";
    String sDate2="31/08/2019";
    String sDate3="30/08/2019";
    String sDate4="29/08/2019";
    String sDate5="25/08/2019";
    String sDate6="30/08/2019";
    String sDate7="25/08/2019";
    String sDate8="26/08/2019";

    Date date1=new SimpleDateFormat("dd/MM/yyyy").parse(sDate1);
    Date date2=new SimpleDateFormat("dd/MM/yyyy").parse(sDate2);
    Date date3=new SimpleDateFormat("dd/MM/yyyy").parse(sDate3);
    Date date4=new SimpleDateFormat("dd/MM/yyyy").parse(sDate4);
    Date date5=new SimpleDateFormat("dd/MM/yyyy").parse(sDate5);
    Date date6=new SimpleDateFormat("dd/MM/yyyy").parse(sDate6);
    Date date7=new SimpleDateFormat("dd/MM/yyyy").parse(sDate7);
    Date date8=new SimpleDateFormat("dd/MM/yyyy").parse(sDate8);



    FlightDetails flight1 = new FlightDetails(1001,"Hyderabad","Delhi","Boeing 777-200LR(77L)",date1);
    FlightDetails flight2 = new FlightDetails(1002,"Chennai","Bangalore","Airbus A319 V2",date2);
    FlightDetails flight3 = new FlightDetails(1003,"Delhi","Bangalore","Airbus A321",date3);
    FlightDetails flight4 = new FlightDetails(1004,"Hyderabad","Pune","Boeing 777-200LR(77L)",date4);
    FlightDetails flight5 = new FlightDetails(1005,"Chennai","Delhi","Airbus A319 V2",date5);
    FlightDetails flight6 = new FlightDetails(1006,"Delhi","Bangalore","Airbus A319 V2",date6);
    FlightDetails flight7 = new FlightDetails(1007,"Pune","Bangalore","Airbus A319 V2",date7);
    FlightDetails flight8 = new FlightDetails(1008,"Delhi","Chennai","Airbus A321",date8);
    FlightRoute route1 = new FlightRoute(1001,date1,6000,13000,20000);
    FlightRoute route2 = new FlightRoute(1002,date2,4000);
    FlightRoute route3 = new FlightRoute(1003,date3,5000,10000,0);
    FlightRoute route4 = new FlightRoute(1004,date4,6000,13000,20000);
    FlightRoute route5 = new FlightRoute(1005,date5,4000);
    FlightRoute route6 = new FlightRoute(1006,date6,4000);
    FlightRoute route7 = new FlightRoute(1007,date7,4000);
    FlightRoute route8 = new FlightRoute(1008,date8,5000,10000);
    public FlightRepository() throws ParseException {
        this.flight = new ArrayList<String>();
        this.city = new ArrayList<String>();
        addCities();
        addFlights();
        addSourceDestinationMapping();
        addTravelClasses();
    }
    public void addFlights(){
        flight.add("Boeing 777-200LR(77L)");
        flight.add("Airbus A319 V2");
        flight.add("Airbus A321");
    }
    public void addCities(){
        city.add("Hyderabad");
        city.add("Chennai");
        city.add("Pune");
        city.add("Delhi");
        city.add("Bangalore");
    }
    public void addSourceDestinationMapping(){

        sourceDestinationFlights = new ArrayList<FlightDetails>();
        sourceDestinationFlights.add(flight1);
        sourceDestinationFlights.add(flight2);
        sourceDestinationFlights.add(flight3);
        sourceDestinationFlights.add(flight4);
        sourceDestinationFlights.add(flight5);
        sourceDestinationFlights.add(flight6);
        sourceDestinationFlights.add(flight7);
        sourceDestinationFlights.add(flight8);
    }
    public List<FlightDetails> retrieveSourceDestinationMapping(){
        //System.out.println(sourceDestinationFlights);
        return sourceDestinationFlights;
    }



    public List<String> retrieveCities(){
       // System.out.println(city);
        return city;
    }
    public List<String> retrieveFlights(){
        return flight;
    }


    TravelClass classPassenger1 = new TravelClass("Boeing 777-200LR(77L)","economy",20,6000,10);
    TravelClass classPassenger2 = new TravelClass("Boeing 777-200LR(77L)","Business",35,13000,35);
    TravelClass classPassenger3 = new TravelClass("Boeing 777-200LR(77L)","FirstClass",8,20000,8);
    TravelClass classPassenger4 = new TravelClass("Airbus A319 V2","economy",144,4000,144);
    TravelClass classPassenger5 = new TravelClass("Airbus A321","economy",152,5000,152);
    TravelClass classPassenger6 = new TravelClass("Airbus A321","Business",20,10000,20);

    public void addTravelClasses(){

        travelClassList.add(classPassenger1);
        travelClassList.add(classPassenger2);
        travelClassList.add(classPassenger3);
        travelClassList.add(classPassenger4);
        travelClassList.add(classPassenger5);
        travelClassList.add(classPassenger6);
    }
    public List<TravelClass> retrieveTravelClasses(){

        return travelClassList;
    }
    public void setFlightRouteData()
    {

    }


}

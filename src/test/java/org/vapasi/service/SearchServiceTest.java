package org.vapasi.service;

import org.vapasi.model.FlightDetails;
import org.junit.Test;
import org.mockito.Mock;
import org.vapasi.model.TravelClass;
import org.vapasi.repository.FlightRepository;

import java.awt.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class SearchServiceTest {
    Map<FlightDetails,String> mockedMap;
    public List<FlightDetails> returnFlightValues() throws ParseException {
        String sDate1="31/08/2019";
        String sDate2="31/08/2019";
        String sDate3="30/08/2019";
        String sDate4="29/08/2019";
        String sDate5="25/08/2019";
        String sDate6="30/08/2019";
        String sDate7="25/08/2019";
        String sDate8="26/08/2019";

        Date date1=new SimpleDateFormat("dd/MM/yyyy").parse(sDate1);
        Date date2=new SimpleDateFormat("dd/MM/yyyy").parse(sDate2);
        Date date3=new SimpleDateFormat("dd/MM/yyyy").parse(sDate3);
        Date date4=new SimpleDateFormat("dd/MM/yyyy").parse(sDate4);
        Date date5=new SimpleDateFormat("dd/MM/yyyy").parse(sDate5);
        Date date6=new SimpleDateFormat("dd/MM/yyyy").parse(sDate6);
        Date date7=new SimpleDateFormat("dd/MM/yyyy").parse(sDate7);
        Date date8=new SimpleDateFormat("dd/MM/yyyy").parse(sDate8);

        FlightDetails flight1 = new FlightDetails(1001,"Hyderabad","Delhi","Boeing 777-200LR(77L)",date1);
        FlightDetails flight2 = new FlightDetails(1002,"Chennai","Bangalore","Airbus A319 V2",date2);
        FlightDetails flight3 = new FlightDetails(1003,"Delhi","Bangalore","Airbus A321",date3);
        FlightDetails flight4 = new FlightDetails(1004,"Hyderabad","Pune","Boeing 777-200LR(77L)",date4);
        FlightDetails flight5 = new FlightDetails(1005,"Chennai","Delhi","Airbus A319 V2",date5);
        FlightDetails flight6 = new FlightDetails(1006,"Delhi","Bangalore","Airbus A319 V2",date6);
        FlightDetails flight7 = new FlightDetails(1007,"Pune","Bangalore","Airbus A319 V2",date7);
        FlightDetails flight8 = new FlightDetails(1008,"Delhi","Chennai","Airbus A321",date8);

        List<FlightDetails> flights = new ArrayList<>();
        flights.add(flight1);
        flights.add(flight2);
        flights.add(flight3);
        flights.add(flight4);
        flights.add(flight5);
        flights.add(flight6);
        flights.add(flight7);
        flights.add(flight8);
        //System.out.println(flights);
        return flights;
    }
    public List<TravelClass> retrieveTravelClasses(){

        TravelClass classPassenger1 = new TravelClass("Boeing 777-200LR(77L)","economy",20,6000,8);
        TravelClass classPassenger2 = new TravelClass("Boeing 777-200LR(77L)","Business",35,13000,35);
        TravelClass classPassenger3 = new TravelClass("Boeing 777-200LR(77L)","FirstClass",8,20000,8);
        TravelClass classPassenger4 = new TravelClass("Airbus A319 V2","economy",144,4000,144);
        TravelClass classPassenger5 = new TravelClass("Airbus A321","economy",152,5000,14);
        TravelClass classPassenger6 = new TravelClass("Airbus A321","Business",20,10000,20);
        List<TravelClass> travelClassList = new ArrayList<>();
        travelClassList.add(classPassenger1);
        travelClassList.add(classPassenger2);
        travelClassList.add(classPassenger3);
        travelClassList.add(classPassenger4);
        travelClassList.add(classPassenger5);
        travelClassList.add(classPassenger6);
        return travelClassList;
    }


    @Mock
    private FlightRepository flightRepository;




    @Test
    public void returnFlightsGivenNumberOfPassengers() throws ParseException {


        //FlightRepository flightRepository = mock(FlightRepository.class);
        when(flightRepository.retrieveSourceDestinationMapping()).thenReturn(returnFlightValues());
        when(flightRepository.retrieveTravelClasses()).thenReturn(retrieveTravelClasses());
        SearchService searchService = new SearchService();
        searchService.setRep(flightRepository);
        Date date = new SimpleDateFormat("dd-MM-yyyy").parse("31-08-2019");
        List<FlightDetails> flights = searchService.returnFlights("Hyderabad","Delhi",3,date,"economy");
        assertEquals(1,flights.size());
    }
    @Test
    public void returnFlightsGivenNoPassengers() throws ParseException {

        FlightRepository flightRepository = mock(FlightRepository.class);
        when(flightRepository.retrieveSourceDestinationMapping()).thenReturn(returnFlightValues());
        when(flightRepository.retrieveTravelClasses()).thenReturn(retrieveTravelClasses());
        SearchService searchService = new SearchService();
        searchService.setRep(flightRepository);
        Date date = new SimpleDateFormat("dd-MM-yyyy").parse("31-08-2019");
        List<FlightDetails> flights = searchService.returnFlights("Hyderabad","Delhi",0,date,"economy");
        assertEquals(1,flights.size());
    }

    @Test
    public void returnFlightsAvailableForNextDayIfDateNotGiven() throws ParseException{

        FlightRepository flightRepository = mock(FlightRepository.class);
        when(flightRepository.retrieveSourceDestinationMapping()).thenReturn(returnFlightValues());
        when(flightRepository.retrieveTravelClasses()).thenReturn(retrieveTravelClasses());
        SearchService searchService = new SearchService();
        searchService.setRep(flightRepository);
        List<FlightDetails> flights = searchService.returnFlights("Hyderabad","Delhi",1,null,"economy");
        assertEquals(1,flights.size());
    }

    @Test
    public void returnFlightsForGivenDepartureDate() throws ParseException{
        FlightRepository flightRepository = mock(FlightRepository.class);
        when(flightRepository.retrieveSourceDestinationMapping()).thenReturn(returnFlightValues());
        when(flightRepository.retrieveTravelClasses()).thenReturn(retrieveTravelClasses());
        SearchService searchService = new SearchService();
        searchService.setRep(flightRepository);
        Date date = new SimpleDateFormat("dd-MM-yyyy").parse("24-08-2019");
        List<FlightDetails> flights = searchService.returnFlights("Hyderabad","Delhi",1,date,"economy");
        assertEquals(1,flights.size());
    }
    @Test
    public void returnFlightsForGivenClassWithSourceDestinationDepartureDateGiven() throws ParseException{
        FlightRepository flightRepository = mock(FlightRepository.class);
        when(flightRepository.retrieveSourceDestinationMapping()).thenReturn(returnFlightValues());
        when(flightRepository.retrieveTravelClasses()).thenReturn(retrieveTravelClasses());
        SearchService searchService = new SearchService();
        searchService.setRep(flightRepository);
        Date date = new SimpleDateFormat("dd-MM-yyyy").parse("30-08-2019");
        List<FlightDetails> flights = searchService.returnFlights("Delhi","Bangalore",1,date,"economy");
        assertEquals(2,flights.size());
    }

    @Test
    public void returnFlightsForSourceDestinationDepartureDateWithoutClassGiven() throws ParseException{
        FlightRepository flightRepository = mock(FlightRepository.class);
        when(flightRepository.retrieveSourceDestinationMapping()).thenReturn(returnFlightValues());
        when(flightRepository.retrieveTravelClasses()).thenReturn(retrieveTravelClasses());
        SearchService searchService = new SearchService();
        searchService.setRep(flightRepository);
        Date date = new SimpleDateFormat("dd-MM-yyyy").parse("30-08-2019");
        List<FlightDetails> flights = searchService.returnFlights("Delhi","Bangalore",1,date,null);
        assertEquals(2,flights.size());
    }

    @Test
    public void returnTotalPriceForGivenPassengers() throws ParseException{
        FlightRepository flightRepository = mock(FlightRepository.class);
        when(flightRepository.retrieveSourceDestinationMapping()).thenReturn(returnFlightValues());
        when(flightRepository.retrieveTravelClasses()).thenReturn(retrieveTravelClasses());
        SearchService searchService = new SearchService();
        searchService.setRep(flightRepository);
        Date date = new SimpleDateFormat("dd-MM-yyyy").parse("24-08-2019");
        List<FlightDetails> flights = searchService.returnFlights("Hyderabad","Delhi",1,date,"economy");
        for(FlightDetails x:flights){
            FlightDetails f = new FlightDetails(1001,x.getSource(),x.getDestination(),x.getFlightName(),x.getDepartureDate());
            double totalFare = searchService.costOfBooking(x);
            assertEquals(6000,totalFare,4);
        }

    }
    @Test
    public void returnTotalPriceForGivenPassengersForEconomyForNext50percentSeats() throws ParseException{
        FlightRepository flightRepository = mock(FlightRepository.class);
        when(flightRepository.retrieveSourceDestinationMapping()).thenReturn(returnFlightValues());
        when(flightRepository.retrieveTravelClasses()).thenReturn(retrieveTravelClasses());
        SearchService searchService = new SearchService();
        searchService.setRep(flightRepository);
        Date date = new SimpleDateFormat("dd-MM-yyyy").parse("24-08-2019");
        List<FlightDetails> flights = searchService.returnFlights("Hyderabad","Delhi",2,date,"economy");
        for(FlightDetails x:flights){
            FlightDetails f = new FlightDetails(1001,x.getSource(),x.getDestination(),x.getFlightName(),x.getDepartureDate());
            double totalFare = searchService.costOfBooking(x);
            assertEquals(15600,totalFare,4);
        }
    }
    @Test
    public void returnTotalPriceForGivenPassengersForEconomyForFirst40percentSeats() throws ParseException {
        FlightRepository flightRepository = mock(FlightRepository.class);
        when(flightRepository.retrieveSourceDestinationMapping()).thenReturn(returnFlightValues());
        when(flightRepository.retrieveTravelClasses()).thenReturn(retrieveTravelClasses());
        SearchService searchService = new SearchService();
        searchService.setRep(flightRepository);
        Date date = new SimpleDateFormat("dd-MM-yyyy").parse("31-08-2019");
        List<FlightDetails> flights = searchService.returnFlights("Chennai", "Bangalore", 2, date, "economy");
        for (FlightDetails x : flights) {
            FlightDetails f = new FlightDetails(1001, x.getSource(), x.getDestination(), x.getFlightName(), x.getDepartureDate());
            double totalFare = searchService.costOfBooking(x);
            assertEquals(8000, totalFare, 4);
        }
    }
    @Test
    public void returnTotalPriceForGivenPassengersForEconomyForLast10percentSeats() throws ParseException {
        FlightRepository flightRepository = mock(FlightRepository.class);
        when(flightRepository.retrieveSourceDestinationMapping()).thenReturn(returnFlightValues());
        when(flightRepository.retrieveTravelClasses()).thenReturn(retrieveTravelClasses());
        SearchService searchService = new SearchService();
        searchService.setRep(flightRepository);
        Date date = new SimpleDateFormat("dd-MM-yyyy").parse("26-08-2019");
        List<FlightDetails> flights = searchService.returnFlights("Delhi", "Chennai", 2, date, "economy");
        for (FlightDetails x : flights) {
            FlightDetails f = new FlightDetails(1008, x.getSource(), x.getDestination(), x.getFlightName(), x.getDepartureDate());
            double totalFare = searchService.costOfBooking(x);
            assertEquals(16000, totalFare, 4);
        }
    }
    @Test
    public void returnTotalPriceForGivenPassengersForBusinessForMoFriOrSun() throws ParseException {
        FlightRepository flightRepository = mock(FlightRepository.class);
        when(flightRepository.retrieveSourceDestinationMapping()).thenReturn(returnFlightValues());
        when(flightRepository.retrieveTravelClasses()).thenReturn(retrieveTravelClasses());
        SearchService searchService = new SearchService();
        searchService.setRep(flightRepository);
        Date date = new SimpleDateFormat("dd-MM-yyyy").parse("30-08-2019");
        List<FlightDetails> flights = searchService.returnFlights("Delhi", "Bangalore", 2, date, "Business");
        for (FlightDetails x : flights) {
            FlightDetails f = new FlightDetails(1008, x.getSource(), x.getDestination(), x.getFlightName(), x.getDepartureDate());
            double totalFare = searchService.costOfBooking(x);
            assertEquals(28000, totalFare, 4);
        }
    }
    @Test
    public void returnTotalPriceForGivenPassengersForBusinessForTueWedThurSat() throws ParseException {
        FlightRepository flightRepository = mock(FlightRepository.class);
        when(flightRepository.retrieveSourceDestinationMapping()).thenReturn(returnFlightValues());
        when(flightRepository.retrieveTravelClasses()).thenReturn(retrieveTravelClasses());
        SearchService searchService = new SearchService();
        searchService.setRep(flightRepository);
        Date date = new SimpleDateFormat("dd-MM-yyyy").parse("29-08-2019");
        List<FlightDetails> flights = searchService.returnFlights("Hyderabad", "Pune", 2, date, "Business");
        for (FlightDetails x : flights) {
            FlightDetails f = new FlightDetails(1008, x.getSource(), x.getDestination(), x.getFlightName(), x.getDepartureDate());
            double totalFare = searchService.costOfBooking(x);
            assertEquals(26000, totalFare, 4);
        }
    }
    @Test
    public void returnNullIfBookingForFirstClassIsDoneMoreThanBefore10DaysOfDeparture() throws ParseException {
        FlightRepository flightRepository = mock(FlightRepository.class);
        when(flightRepository.retrieveSourceDestinationMapping()).thenReturn(returnFlightValues());
        when(flightRepository.retrieveTravelClasses()).thenReturn(retrieveTravelClasses());
        SearchService searchService = new SearchService();
        searchService.setRep(flightRepository);
        Date date = new SimpleDateFormat("dd-MM-yyyy").parse("04-09-2019");
        List<FlightDetails> flights = searchService.returnFlights("Hyderabad", "Delhi", 2, date, "FirstClass");
        for (FlightDetails x : flights) {
            FlightDetails f = new FlightDetails(1008, x.getSource(), x.getDestination(), x.getFlightName(), x.getDepartureDate());

            //double totalFare = searchService.costOfBooking(x);
            assertEquals(0, flights.size(), 4);
        }
    }
    @Test
    public void returnTotalFareForFirstClassIfBookingIsDoneBefore0To10DaysOfDeparture() throws ParseException {
        FlightRepository flightRepository = mock(FlightRepository.class);
        when(flightRepository.retrieveSourceDestinationMapping()).thenReturn(returnFlightValues());
        when(flightRepository.retrieveTravelClasses()).thenReturn(retrieveTravelClasses());
        SearchService searchService = new SearchService();
        searchService.setRep(flightRepository);
        Date date = new SimpleDateFormat("dd-MM-yyyy").parse("31-08-2019");
        List<FlightDetails> flights = searchService.returnFlights("Hyderabad", "Delhi", 2, date, "FirstClass");
        for (FlightDetails x : flights) {
            FlightDetails f = new FlightDetails(1008, x.getSource(), x.getDestination(), x.getFlightName(), x.getDepartureDate());

            double totalFare = searchService.costOfBooking(x);
            assertEquals(52000, totalFare, 4);
        }
    }
}
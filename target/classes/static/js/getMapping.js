$(document).ready(function () {

    $("#search").click(function (event) {
        console.log("---->",event);

        //stop submit the form, we will post it manually.
        event.preventDefault();

        fire_ajax_submit();

    });

});

function fire_ajax_submit() {

    var data = {}
    data["source"] = $("#source").val();
    data["destination"] = $("#destination").val();
    data["noOfPassengers"] = $("#noOfPassengers").val();
    data["departureDate"] = $("#departureDate").val();
    data["classOfTravel"] =  $("[name='classOfTravel']:checked").val();

    $("#search").prop("disabled", true);

    $.ajax({
        type: "POST",
        contentType: "application/json",
        url: "/flights/search",
        data: JSON.stringify(data),
        dataType: 'json',
        cache: false,
        timeout: 600000,
        success: function (data) {

        /*    var json = "<h4>Ajax Response</h4><pre>"
                + JSON.stringify(data, null, 4) + "</pre>";
            $('#list').html(json);


            console.log("SUCCESS : ", data);
            $("#search").prop("disabled", false);

        },*/
        //function createTableFromJSON() {

        console.log("SUCCESS:",data);
            var flightsData = data.result;

        console.log("abcd");
        var col = [];
        for (var i = 0; i < flightsData.length; i++) {
            for (var key in flightsData[i]) {
                if (col.indexOf(key) === -1) {
                    col.push(key);
                }
            }
        }
        var table = document.createElement("table");
        var tr = table.insertRow(-1);
        for (var i = 0; i < col.length; i++) {
            var th = document.createElement("th");
            th.innerHTML = col[i];
            tr.appendChild(th);
        }
        for (var i = 0; i < flightsData.length; i++) {
            tr = table.insertRow(-1);
            for (var j = 0; j < col.length; j++) {
                var tabCell = tr.insertCell(-1);
                tabCell.innerHTML = flightsData[i][col[j]];
            }
        }
        var divContainer = document.getElementById("list");
        divContainer.innerHTML = "";
        divContainer.appendChild(table);
            $("#search").prop("disabled", false);
    },

        error: function (e) {

            var json = "<h4>Ajax Response</h4><pre>"
                + e.responseText + "</pre>";
            $('#list').html(json);

            console.log("ERROR : ", e);
            $("#search").prop("disabled", false);

        }
    });


}